package com.rummyday.payments.utils;

import org.springframework.core.env.Environment;

public class EnvironmentUtils {
	
	public static boolean hasProdProfile(Environment env) {
		return hasProfile(env, "prod");
	}
	
	public static boolean hasProfile(Environment env, String searchProfile) {
		String[] profiles = env.getActiveProfiles();
		if(profiles != null && profiles.length > 0) {
			for(String profile: profiles) {
				if(profile.equalsIgnoreCase(searchProfile))
					return true;
			}
		}
		return false;
	}

}
