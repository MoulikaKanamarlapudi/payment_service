package com.rummyday.payments.utils;

import java.nio.ByteBuffer;
import java.util.UUID;

import io.seruco.encoding.base62.Base62;

public class Base62OrderGenerator implements OrderIdGenerator {
	
	private static Base62 base62 = Base62.createInstance();

	@Override
	public String generateNew() {
		UUID randomUUID = UUID.randomUUID();
		ByteBuffer bb = ByteBuffer.allocate(Long.BYTES * 2);
		bb.putLong(randomUUID.getMostSignificantBits());
		bb.putLong(randomUUID.getLeastSignificantBits());
		return new String(base62.encode(bb.array()));
	}
}
