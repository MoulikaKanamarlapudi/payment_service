package com.rummyday.deposits.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

@JsonInclude(Include.NON_NULL)
public class InitiatePaymentResponseDTO {

	private String gatewayURL;

	private int gatewayId;
	
	private long id;
	
	@JsonUnwrapped
	private Object gatewayRequestParams;

	public String getGatewayURL() {
		return gatewayURL;
	}

	public void setGatewayURL(String gatewayURL) {
		this.gatewayURL = gatewayURL;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Object getGatewayRequestParams() {
		return gatewayRequestParams;
	}

	public void setGatewayRequestParams(Object gatewayRequestParams) {
		this.gatewayRequestParams = gatewayRequestParams;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
