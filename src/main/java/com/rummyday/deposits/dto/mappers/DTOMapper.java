package com.rummyday.deposits.dto.mappers;

import com.rummyday.deposits.domain.models.DepositRequest;
import com.rummyday.deposits.dto.ApiErrorCodes;
import com.rummyday.deposits.dto.InitiatePaymentsDTO;
import com.rummyday.deposits.validators.ValidationException;
import com.rummyday.deposits.validators.ValidationResult;
import com.rummyday.deposits.validators.ValidatorRegistry;

public class DTOMapper {
	
	public  static DepositRequest map(InitiatePaymentsDTO initiatePaymentsDTO) throws ValidationException {
		ValidationResult<Integer> result = ValidatorRegistry.<Integer>getValidator(ValidatorRegistry.DEPOSIT_AMOUNT)
				.isValid(initiatePaymentsDTO.getOrderAmount());
		if (result.isValueTransformed()) {
			initiatePaymentsDTO.setOrderAmount(result.getValue());
		}

		validateUserId(initiatePaymentsDTO.getUserId());

		DepositRequest p = new DepositRequest();
		p.setAmount(initiatePaymentsDTO.getOrderAmount());
		p.setUserId(initiatePaymentsDTO.getUserId());
		return p;
	}

	public static void validateUserId(Long userId) {
		if (userId == null || userId <= 0) {
			throw new ValidationException(
					new ValidationResult<String>("Userid is not provided", ApiErrorCodes.USERID_NOT_PROVIDED));
		}
	}

}
