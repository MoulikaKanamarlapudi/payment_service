package com.rummyday.deposits.dto;

public interface ApiErrorCodes {
	public static final String AMOUNT_REQUIRED = "AMOUNT_REQUIRED";
	public static final String INVALID_AMOUNT_RANGE = "INVALID_AMOUNT_RANGE";
	public static final String USERID_NOT_PROVIDED = "USERID_NOT_PROVIDED";
	public static final String INVALID_USERID = "INVALID_USERID";
	
	public static final String CANT_READ_INPUT_MESSAGE = "CANT_READ_INPUT_MESSAGE";
	public static final String INVALID_API_METHOD_ARGUMENTS = "INVALID_API_METHOD_ARGUMENTS";
	public static final String CASHFREE_SIGNATURE_ERROR = "CASHFREE_SIGNATURE_ERROR";
	public static final String INVALID_PAYMENT_GATEWAY_ID = "INVALID_PAYMENT_GATEWAY_ID";
}