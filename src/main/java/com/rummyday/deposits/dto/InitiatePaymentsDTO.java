package com.rummyday.deposits.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InitiatePaymentsDTO extends ContextAwareDTO {
	
	@JsonProperty("user_id")
	private Long userId;
	
	private int orderAmount;
	
	private String promoCode;
	
	private Integer gatewayId;

	public Long getUserId() {
		return userId;
	}

	public int getOrderAmount() {
		return orderAmount;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}
}