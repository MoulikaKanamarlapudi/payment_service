package com.rummyday.deposits.dto;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContextAwareDTO {
	
	private static final ThreadLocal<Map<String, String>> context = new ThreadLocal<>();
	
	public ContextAwareDTO() {
		context.remove();
	}
	
	public static String getContext(String name) {
		Map<String, String> params = context.get();
		if(params != null) 
			return params.get(name);
		return null;
	}
	
	public static void addToContext(String name, String value) {
		Map<String, String> currentContext = context.get();
		if(currentContext == null) {
			currentContext = new LinkedHashMap<>();
			context.set(currentContext);
		}
		currentContext.put(name, value);
	}
	
	public static void addToContext(Map<String, String> map) {
		if(map != null ) {
			Map<String, String> currentContext = context.get();
			if(currentContext == null) {
				currentContext = new LinkedHashMap<>();
				context.set(currentContext);
			}
			currentContext.putAll(map);
		}		
	}

	@JsonProperty("context")
	private void unpackNested(Map<String, String> contextObj) {
		ContextAwareDTO.context.set(contextObj);
	}
	
	public static Map<String, String> getCurrentContext() {
		return ContextAwareDTO.context.get();
	}
	
	public static Map<String, String> getAndClearCurrentContext() {
		Map<String, String> map = ContextAwareDTO.context.get();
		if(map != null)
			ContextAwareDTO.context.remove();
		return map;
	}
	
	public static void clearCurrentContext() {
		ContextAwareDTO.context.set(null);
	}
}