package com.rummyday.deposits.config.zookeeper;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ZooKeeperConfig extends GenericConfig implements Watcher
{
	private static Logger logger = LoggerFactory.getLogger( ZooKeeperConfig.class );
	private boolean watchFlag;
	private ZooKeeper zooKeeper;
	private String environment;
	private String appname;
	private String myIp;
	private String serviceDetails;
	private String envServiceRoot;
	private String envConfigRoot;
	public static final String CONFIG_ROOT = "/config";
	public static final String SERVICE_ROOT = "/services";
	public static final String ENABLED = "enabled";
	public static final String DISABLED = "disabled";

	@Autowired
	private Environment env;

	@SuppressWarnings("unused")
	private Properties properties = null;

	@PostConstruct
	private void init() {
		initZooKeeper();
		registerAsAServiceOnZK();
		loadPropertiesFromZookeeper();
	}

	public boolean isWatchFlag()
	{
		return watchFlag;
	}

	public String getEnvServiceRoot()
	{
		return envServiceRoot;
	}

	public String getEnvironment()
	{
		return environment;
	}

	public String getAppname()
	{
		return appname;
	}

	private String getEnvConfigRoot()
	{
		return envConfigRoot;
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	private void createNodeByte( String path, byte[] data ) throws KeeperException, InterruptedException
	{
		zooKeeper.create( path, data, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT );
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public void createNodeString( String path, String data ) throws KeeperException, InterruptedException
	{
		byte[] byteData = data.getBytes();
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		if( status != null )
			updateDataAsByte( path, byteData );
		else
			createNodeByte( path, byteData );
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public String readString( String path ) throws KeeperException, InterruptedException
	{
		byte[] data = readByte( path );
		return new String( data );
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public void updateDataAsString( String path, String data ) throws KeeperException, InterruptedException
	{
		byte[] byteData = data.getBytes();
		updateDataAsByte( path, byteData );
	}

	/**
	 * @param path
	 * @throws InterruptedException
	 * @throws KeeperException
	 */
	public void deleteNode( String path ) throws InterruptedException, KeeperException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		zooKeeper.delete( path, status.getVersion() );
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public List< String > getChildren( String path ) throws KeeperException, InterruptedException
	{
		List< String > znodeList = zooKeeper.getChildren( path, isWatchFlag() );
		return znodeList;
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public boolean exists( String path ) throws KeeperException, InterruptedException
	{
		if( zooKeeper.exists( path, isWatchFlag() ) != null )
			return true;
		else
			return false;
	}

	private byte[] readByte( String path ) throws KeeperException, InterruptedException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		byte[] data = zooKeeper.getData( path, isWatchFlag(), status );
		return data;
	}

	private void updateDataAsByte( String path, byte[] data ) throws KeeperException, InterruptedException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		zooKeeper.setData( path, data, status.getVersion() );
	}

	@PreDestroy
	public void cleanup()
	{
		if( zooKeeper == null || zooKeeper.getState() == ZooKeeper.States.CLOSED )
		{
			logger.debug( "-----------------------ZK already closed----------------------------" );
			return;
		}
		try
		{
			zooKeeper.close();
			logger.debug( "-------------------------Closed ZK connection------------------------" );
		}
		catch( Exception e )
		{
			logger.error( "Exception in cleanup.", e );
		}
	}

	public void deRegisterServiceFromZookeeper()
	{
		try
		{
			updateDataAsString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, DISABLED + "," + serviceDetails );
		}
		catch( Exception e )
		{
			logger.error( "Exception in deregistration of service.", e );
		}
	}

	public void setWatchFlag( boolean watchFlag )
	{
		this.watchFlag = watchFlag;
	}

	public Properties loadProperties()
	{
		Properties properties = new Properties();
		try
		{
			System.out.println( "loadProperties environmnet " + environment + " CONFIG_ROOT " +  CONFIG_ROOT + " appname " + appname );
			String appPath = CONFIG_ROOT + "/" + appname;
			List< String > props = getChildren( appPath );
			System.out.println( "properties loaded " + props.toString() );
			logger.debug( "loadProperties " + props.toString() );
			for( String propName : props )
			{
				String propPath = appPath + "/" + propName;
				String propValue = readString( propPath );
				properties.put( propName, propValue );
			}
		}
		catch( Exception e )
		{
			logger.error( "Exception in zk load props:" + e );
		}
		return properties;
	}

	@Override
	public void process( WatchedEvent event )
	{
		try
		{
			logger.info( "====Zookeeper properties changed===>>>" + event.toString() );
			if( event.getPath() != null && event.getPath().contains( getEnvConfigRoot() ) )
			{
				logger.info( "Node changed:" + event.getPath() + " , " + event.getState() + " , " + event.toString() );
				if( watchFlag )
				{
					logger.info( "Watchflag true, reloading properties" );
					reload();
				}
				else
				{
					logger.info( "Watchflag false, not reloading properties" );
				}
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private String getPropertyName( String fullNodePath )
	{
		String propertyName = null;
		try
		{
			String tokens[] = fullNodePath.split( "/" );
			return tokens[tokens.length - 1];
		}
		catch( Exception e )
		{
			logger.error( "Exception in getPropertyName.", e );
		}
		return propertyName;
	}

	private void registerAsAServiceOnZK()
	{
		try
		{
			if( !exists( SERVICE_ROOT ) )
				createNodeString( SERVICE_ROOT, "services" );
			if( !exists( SERVICE_ROOT + "/" + environment ) )
				createNodeString( SERVICE_ROOT + "/" + environment, "environment" );
			if( !exists( SERVICE_ROOT + "/" + environment + "/" + appname ) )
				createNodeString( SERVICE_ROOT + "/" + environment + "/" + appname, "app name" );
			if( !exists( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp ) )
				createNodeString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, ENABLED + "," + serviceDetails );
			else
				updateDataAsString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, ENABLED + "," + serviceDetails );

		}
		catch( Exception e )
		{
			logger.error( "Exception in registerAsAServiceOnZK.", e );
		}
	}

	private void loadPropertiesFromZookeeper()
	{
		Properties properties = loadProperties();
		logger.debug( "Properties initialized:" + properties );
		setProperties( properties );
	}


	private void initZooKeeper()
	{
		System.out.println("env " + env);
		watchFlag = Boolean.parseBoolean(env.getProperty( "zookeeper.watchFlag", "true" ));
		environment = env.getProperty( "zookeeper.environment", "test" );
		appname = env.getProperty( "zookeeper.appname", "payments" );
		serviceDetails = env.getProperty( "zookeeper.serviceDetails", "1234,abc" );
		myIp = null;

		String zkHost = env.getProperty( "zookeeper.zkHost", "localhost" );
		int sessionTimeout = Integer.parseInt( env.getProperty( "zookeeper.sessionTimeout", "10000" ) );

		try
		{
			InetAddress ipAddr = InetAddress.getLocalHost();
			myIp = ipAddr.getHostAddress();
		}
		catch( UnknownHostException ex )
		{
			ex.printStackTrace();
		}
		try
		{
			zooKeeper = new ZooKeeper( zkHost, sessionTimeout, this );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		envServiceRoot = "/" + environment + SERVICE_ROOT + "/" + appname;
		envConfigRoot = "/" + environment + CONFIG_ROOT + "/" + appname;

		System.out.println( "watchFlag " + watchFlag + " zkHost " + zkHost + " sessionTimeout " + sessionTimeout + " environment " + environment + " appname " + appname + " serviceDetails "
				+ serviceDetails );
	}
	public void reload()
	{
		loadPropertiesFromZookeeper();
	}
	
	public String getZKProperty(String propName) {
		String appPath = CONFIG_ROOT + "/" + appname;
		try {
			
			boolean isExits = getChildren(appPath).contains(propName);
			if (isExits) {
				String propPath = appPath + "/" + propName;
				return readString(propPath);
			} else {
				return null;
			}
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	
	public void setZKProperty(String propName,String propValue) {
		String appPath = CONFIG_ROOT + "/" + appname;
		try {
			String path = appPath +"/"+propName;
			createNodeString(path, propValue);						
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void deleteProperty(String propName) {
		String appPath = CONFIG_ROOT + "/" + appname;
		try {
			String path = appPath +"/"+propName;
			deleteNode(path);						
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
