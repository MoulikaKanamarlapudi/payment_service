package com.rummyday.deposits.config.zookeeper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public abstract class GenericConfig implements Configuration
{

	private Properties properties = null;
	
	public abstract void reload();

	public String getStringValue( String configParamKey )
	{
		return properties.getProperty( configParamKey );
	}

	public String getStringValue( String configParamKey, String defaultValue )
	{
		String property = properties.getProperty( configParamKey );
		return property == null ? defaultValue : property;
	}

	public String[] getCSVStringValues( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		String[] values = null;
		if( property != null )
		{
			values = property.split( "," );
			for( int i = 0; i < values.length; i++ )
			{
				values[i] = values[i].trim();
			}
		}
		return values;
	}

	public String[] getCSVStringValues( String configParamKey, String[] defaultValues )
	{
		String property = getStringValue( configParamKey );
		String[] values = null;
		if( property != null )
		{
			values = property.split( "," );
			for( int i = 0; i < values.length; i++ )
			{
				values[i] = values[i].trim();
			}
		}
		return values == null ? defaultValues : values;
	}

	public Set< String > getCSVStringValuesAsSet( String configParamKey )
	{
		return new HashSet< String >( getCSVStringValuesAsList( configParamKey ) );
	}

	public Set< String > getCSVStringValuesAsSet( String configParamKey, Set< String > defaultValues )
	{
		return new HashSet< String >( Arrays.asList( getCSVStringValues( configParamKey, defaultValues.toArray( new String[defaultValues.size()] ) ) ) );
	}

	public List< String > getCSVStringValuesAsList( String configParamKey )
	{
		return Arrays.asList( getCSVStringValues( configParamKey, new String[0] ) );
	}

	public List< String > getCSVStringValuesAsList( String configParamKey, List< String > defaultValues )
	{
		return Arrays.asList( getCSVStringValues( configParamKey, defaultValues.toArray( new String[defaultValues.size()] ) ) );
	}

	public boolean getBooleanValue( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		boolean booleanValue = false;
		if( property != null )
		{
			booleanValue = Boolean.parseBoolean( property );
		}
		return booleanValue;
	}

	public boolean getBooleanValue( String configParamKey, boolean defaultValue )
	{
		String property = getStringValue( configParamKey );
		boolean booleanValue = defaultValue;
		if( property != null )
		{
			booleanValue = Boolean.parseBoolean( property );
		}
		return booleanValue;
	}

	public int getIntValue( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? Integer.MIN_VALUE : Integer.parseInt( property );
		}
		catch( NumberFormatException e )
		{
			return Integer.MIN_VALUE;
		}
	}

	public int getIntValue( String configParamKey, int defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? defaultValue : Integer.parseInt( property );
		}
		catch( NumberFormatException e )
		{
			return defaultValue;
		}
	}
	
	public int getPositiveIntValue( String configParamKey, int defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			if(property != null) {
				int value = Integer.parseInt(property);
				if(value > 0)
					return value;
			}
		}
		catch( NumberFormatException e )
		{
		}
		return defaultValue;
	}

	public long getLongValue( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? Long.MIN_VALUE : Long.parseLong( property );
		}
		catch( NumberFormatException e )
		{
			return Long.MIN_VALUE;
		}
	}

	public long getLongValue( String configParamKey, long defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? defaultValue : Long.parseLong( property );
		}
		catch( NumberFormatException e )
		{
			return defaultValue;
		}
	}

	public float getFloatValue( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? Float.MIN_VALUE : Float.parseFloat( property );
		}
		catch( NumberFormatException e )
		{
			return Float.MIN_VALUE;
		}
	}

	public float getFloatValue( String configParamKey, float defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? defaultValue : Float.parseFloat( property );
		}
		catch( NumberFormatException e )
		{
			return defaultValue;
		}
	}

	@Override
	public double getDoubleValue( String configParamKey )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? Double.MIN_VALUE : Double.parseDouble( property );
		}
		catch( NumberFormatException e )
		{
			return Float.MIN_VALUE;
		}
	}

	@Override
	public double getDoubleValue( String configParamKey, double defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? defaultValue : Double.parseDouble( property );
		}
		catch( NumberFormatException e )
		{
			return defaultValue;
		}
	}

	public < T extends Enum > T getEnumeratedValue( String configParamKey, Class< T > enumType )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? ( T ) null : ( T ) Enum.valueOf( enumType, property );
		}
		catch( Exception e )
		{
			return null;
		}
	}

	public < T extends Enum > T getEnumeratedValue( String configParamKey, Class< T > enumType, T defaultValue )
	{
		String property = getStringValue( configParamKey );
		try
		{
			return property == null ? defaultValue : ( T ) Enum.valueOf( enumType, property );
		}
		catch( Exception e )
		{
			return defaultValue;
		}
	}

	protected void setProperties( Properties properties )
	{
		this.properties = properties;
	}

	@Override
	public String toString()
	{
		return "PropsFileBasedConfiguration{" + "properties=" + properties + '}';
	}
	
	public void setProperty(String key, String value)
	{
		properties.setProperty( key, value );
	}
}
