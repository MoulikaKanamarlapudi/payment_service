package com.rummyday.deposits.config.zookeeper;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public interface Configuration
{
	public String getStringValue( String configParamKey );

	public String getStringValue( String configParamKey, String defaultValue );

	public String[] getCSVStringValues( String configParamKey );

	public String[] getCSVStringValues( String configParamKey, String[] defaultValues );

	public Set< String > getCSVStringValuesAsSet( String configParamKey );

	public Set< String > getCSVStringValuesAsSet( String configParamKey, Set< String > defaultValues );

	public List< String > getCSVStringValuesAsList( String configParamKey );

	public List< String > getCSVStringValuesAsList( String configParamKey, List< String > defaultValues );

	public boolean getBooleanValue( String configParamKey );

	public boolean getBooleanValue( String configParamKey, boolean defaultValue );

	public int getIntValue( String configParamKey );

	public int getIntValue( String configParamKey, int defaultValue );

	public int getPositiveIntValue( String configParamKey, int defaultValue );

	public long getLongValue( String configParamKey );

	public long getLongValue( String configParamKey, long defaultValue );

	public float getFloatValue( String configParamKey );

	public float getFloatValue( String configParamKey, float defaultValue );

	public double getDoubleValue( String configParamKey );
	
	public void reload();

	public double getDoubleValue( String configParamKey, double defaultValue );

	public < T extends Enum > T getEnumeratedValue( String configParamKey, Class< T > enumType );

	public < T extends Enum > T getEnumeratedValue( String configParamKey, Class< T > enumType, T defaultValue );
}
