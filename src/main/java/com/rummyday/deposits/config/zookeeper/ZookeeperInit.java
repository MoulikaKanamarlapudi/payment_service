package com.rummyday.deposits.config.zookeeper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ZookeeperInit
{
	@Autowired
	private Configuration config;

	public void reloadProperties()
	{
		config.reload();
	}
	
	public Configuration getConfiguration()
	{
		return config;
	}

	public void setConfiguration( Configuration config )
	{
		this.config = config;
	}
}
