package com.rummyday.deposits.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rummyday.deposits.config.zookeeper.ZookeeperInit;
import com.rummyday.deposits.domain.models.GatewayIds;
import com.rummyday.payments.utils.Base62OrderGenerator;
import com.rummyday.payments.utils.OrderIdGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class PaymentServiceConfiguration {

	@Autowired
	ZookeeperInit config;
	
	private String orderCurrency;
	
	public static class DepositLimits {
		private final int minimumAmountToDeposit;
		
		private final int maximumAmountToDeposit;
		
		public DepositLimits(int minimumAmountToDeposit, int maximumAmountToDeposit) {
			this.minimumAmountToDeposit = minimumAmountToDeposit;
			this.maximumAmountToDeposit = maximumAmountToDeposit;
		}

		public int getMinimumAmountToDeposit() {
			return minimumAmountToDeposit;
		}

		public int getMaximumAmountToDeposit() {
			return maximumAmountToDeposit;
		}
	}
	
	public static class CashFreeGatewayConfig {
		private String appId;
		
		private String secretKey;
		
		private String prodURL;
		
		private String testURL;
		
		private String notifyURL;

		public String getAppId() {
			return appId;
		}

		public void setAppId(String appId) {
			this.appId = appId;
		}

		public String getSecretKey() {
			return secretKey;
		}

		public void setSecretKey(String secretKey) {
			this.secretKey = secretKey;
		}

		public String getNotifyURL() {
			return notifyURL;
		}

		public void setNotifyURL(String notifyURL) {
			this.notifyURL = notifyURL;
		}

		public String getProdURL() {
			return prodURL;
		}

		public void setProdURL(String prodURL) {
			this.prodURL = prodURL;
		}

		public String getTestURL() {
			return testURL;
		}

		public void setTestURL(String testURL) {
			this.testURL = testURL;
		}
	}
	
	private DepositLimits depositLimits;
	
	private CashFreeGatewayConfig cashfreeGatewayConfig;
	
	private GatewayIds defaultGateway = GatewayIds.CASHFREE;
	
	private String userDefaultEmail;
	
	@PostConstruct
	public void init() {
		CashFreeGatewayConfig gateWayConfig = new CashFreeGatewayConfig();
		gateWayConfig.appId = config.getConfiguration().getStringValue("CASHFREE_APP_ID", "16553522185847c9f090317f835561");
		gateWayConfig.secretKey = config.getConfiguration().getStringValue("CASHFREE_APP_SECRET_KEY", "15fe7b00f86016728c7fb702a2e6ecf47ed6e6a5");
		gateWayConfig.testURL = config.getConfiguration().getStringValue("CASHFREE_TEST_URL", "https://test.cashfree.com/billpay/checkout/post/submit");
		gateWayConfig.prodURL = config.getConfiguration().getStringValue("CASHFREE_URL", "https://www.cashfree.com/billpay/checkout/post/submit");
		gateWayConfig.notifyURL = config.getConfiguration().getStringValue("CASHFREE_NOTIFY_URL", "http://paymentservice/notifyPayment");
		
		this.cashfreeGatewayConfig = gateWayConfig;
		int minimumAmountToDeposit = config.getConfiguration().getPositiveIntValue("MINIMUM_AMOUNT_FOR_DEPOSIT", 25);
		int maximumAmountToDeposit = config.getConfiguration().getPositiveIntValue("MAXIMUM_AMOUNT_FOR_DEPOSIT", 100000);
		if(maximumAmountToDeposit < minimumAmountToDeposit) {
			log.error("Minimum deposit amount is greater than maximum deposit amount swapping them");
			int temp = maximumAmountToDeposit;
			maximumAmountToDeposit = minimumAmountToDeposit;
			minimumAmountToDeposit = temp;
		}
		this.depositLimits = new DepositLimits(minimumAmountToDeposit, maximumAmountToDeposit);
		String gatewayDefault = config.getConfiguration().getStringValue("DEFAULT_PAYMENT_GATEWAY", GatewayIds.CASHFREE.name()).toUpperCase();
		this.defaultGateway = GatewayIds.valueOf(gatewayDefault);
		log.info(this.defaultGateway.name() + ": is set to the default payment gateway");
		this.orderCurrency = config.getConfiguration().getStringValue("ORDER_CURRENCY", "INR");
		this.userDefaultEmail = config.getConfiguration().getStringValue("PAYMENT_GATEWAY_DEFAULT_EMAIL", "moulika@gamestr.in");
	}

	public String getOrderCurrency() {
		return orderCurrency;
	}

	public DepositLimits getDepositLimits() {
		return depositLimits;
	}	
	
	public CashFreeGatewayConfig getCashFreeGatewayConfig() {
		return cashfreeGatewayConfig;
	}

	public GatewayIds getDefaultGateway() {
		return defaultGateway;
	}
	
	public String getUserDefaultEmail() {
		return userDefaultEmail;
	}

	@Bean
	public OrderIdGenerator orderIdGenerator() {
		return new Base62OrderGenerator();
	}
}