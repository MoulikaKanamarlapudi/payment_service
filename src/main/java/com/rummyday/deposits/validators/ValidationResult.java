package com.rummyday.deposits.validators;

public class ValidationResult<T> {
	
	private final ValidationStatus isValid;
	
	private final String errorMessage;
	
	private final String errorCode;
	
	private final T value;
	
	@SuppressWarnings("rawtypes")
	private static final ValidationResult SUCCESS = new ValidationResult();

	public ValidationStatus getValidationStatus() {
		return isValid;
	}
	
	public boolean isValueTransformed() {
		return isValid == ValidationStatus.VALIDATE_SUCCESS_WITH_TRANSFORM;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public T getValue() {
		return value;
	}

	public ValidationResult(String erroMsg, String errorCode) {
		this.isValid = ValidationStatus.VALIDATE_FAILURE;
		this.errorMessage = erroMsg;
		this.value = null;
		this.errorCode = errorCode;
	}
	
	private ValidationResult() {
		this.isValid = ValidationStatus.VALIDATE_SUCCESS_NO_TRANSFORM;
		this.errorMessage = null;
		this.errorCode = null;
		this.value = null;
	}
	
	private ValidationResult(T value) {
		this.isValid = ValidationStatus.VALIDATE_SUCCESS_WITH_TRANSFORM;
		this.errorMessage = null;
		this.errorCode = null;
		this.value = value;
	}
	
	public static <T> ValidationResult<T> getSuccess(T value) {
		return new ValidationResult<>(value);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> ValidationResult<T> getSuccess() {
		return SUCCESS;
	}
}
