package com.rummyday.deposits.validators;

import java.io.Serializable;

public class ValidationException extends RuntimeException {
	
	private static final long serialVersionUID = 738823094731757275L;
	private ValidationResult<? extends Serializable> validationResult;

	public ValidationException(ValidationResult<? extends Serializable> validationResult) {
		this.validationResult = validationResult;		
	}
	
	public String getMessage() {
		return this.validationResult.getErrorMessage();
	}
	
	public String getLocalizedMessage() {
		return this.validationResult.getErrorMessage();
	}
	
	public String getErrorCode() {
		return this.validationResult.getErrorCode();
	}

}
