package com.rummyday.deposits.validators;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rummyday.deposits.config.PaymentServiceConfiguration;
import com.rummyday.deposits.config.PaymentServiceConfiguration.DepositLimits;
import com.rummyday.deposits.dto.ApiErrorCodes;

@Component
public class DepositAmountValidator implements Validator<Integer> {

	@Autowired
	private PaymentServiceConfiguration paymentServiceConfiguration;
	
	@PostConstruct
	public void addToRegistry( ) {
		ValidatorRegistry.addValidator(ValidatorRegistry.DEPOSIT_AMOUNT, this);
	}

	@Override
	public ValidationResult<Integer> validate(Integer value) {
		if (value == null)
			return new ValidationResult<Integer>("amount must not be blank", ApiErrorCodes.AMOUNT_REQUIRED);
		DepositLimits depositLimits = paymentServiceConfiguration.getDepositLimits();
		int maxDeposit = depositLimits.getMaximumAmountToDeposit();
		int minDeposit = depositLimits.getMinimumAmountToDeposit();
		if (value > maxDeposit && value < minDeposit)
			return new ValidationResult<>(
					String.format("Amount to deposit must between %d and %d", minDeposit, maxDeposit),
					ApiErrorCodes.INVALID_AMOUNT_RANGE);
		return ValidationResult.getSuccess();
	}
}