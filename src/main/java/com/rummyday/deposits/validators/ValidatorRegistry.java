package com.rummyday.deposits.validators;

import java.util.HashMap;
import java.util.Map;

public class ValidatorRegistry {
	
	public static final String DEPOSIT_AMOUNT = "depositAmount";	
	
	private static  Map<String, Validator> registry = new HashMap<>();
	
	public static <T> Validator<T> getValidator(String name) {
		return registry.get(name);
	}
	
	public static <T> void addValidator(String name, Validator<T> validator) {
		registry.put(name, validator);
	}
}
