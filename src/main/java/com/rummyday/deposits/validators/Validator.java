package com.rummyday.deposits.validators;

import java.io.Serializable;

public interface Validator<T> {
	
	public ValidationResult<T> validate(T value);
	
	default public ValidationResult<T> isValid(T value) throws ValidationException {
		ValidationResult<T> result = validate(value);
		if(result.getValidationStatus() == ValidationStatus.VALIDATE_FAILURE)
			throw new ValidationException((ValidationResult<? extends Serializable>) result);
		return result;
	}
}
