package com.rummyday.deposits.validators;

public enum ValidationStatus {
	
	VALIDATE_SUCCESS_NO_TRANSFORM,
	
	VALIDATE_SUCCESS_WITH_TRANSFORM,
	
	VALIDATE_FAILURE

}
