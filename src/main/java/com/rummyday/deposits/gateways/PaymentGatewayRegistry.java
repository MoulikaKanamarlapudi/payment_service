package com.rummyday.deposits.gateways;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rummyday.deposits.domain.models.GatewayIds;

@Component
public class PaymentGatewayRegistry {
	
	@Autowired
	private List<PaymentGateway> paymentGateways;
	
	public PaymentGateway getPaymentGateway(GatewayIds gatewayId) {
		Optional<PaymentGateway> chosenGateway = paymentGateways.stream().filter(gateway -> gateway.getGatewayId() == gatewayId).findFirst();
		if(chosenGateway.isPresent())
			return chosenGateway.get();
		return null;//wont hanppen
	}
}
