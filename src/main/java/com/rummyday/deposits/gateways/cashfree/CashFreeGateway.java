package com.rummyday.deposits.gateways.cashfree;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.rummyday.deposits.config.PaymentServiceConfiguration;
import com.rummyday.deposits.config.PaymentServiceConfiguration.CashFreeGatewayConfig;
import com.rummyday.deposits.domain.models.DepositRequest;
import com.rummyday.deposits.domain.models.GatewayIds;
import com.rummyday.deposits.domain.models.UserDetails;
import com.rummyday.deposits.dto.ApiErrorCodes;
import com.rummyday.deposits.dto.InitiatePaymentResponseDTO;
import com.rummyday.deposits.gateways.PaymentGateway;
import com.rummyday.deposits.gateways.cashfree.exceptions.CashfreeSignaureGenerationException;
import com.rummyday.deposits.gateways.cashfree.repo.UserDetailsRepository;
import com.rummyday.deposits.validators.ValidationException;
import com.rummyday.deposits.validators.ValidationResult;
import com.rummyday.payments.utils.EnvironmentUtils;

@Component
public class CashFreeGateway implements PaymentGateway {

	@Autowired
	PaymentServiceConfiguration paymentServiceConfiguration;

	@Autowired
	private Environment environment;

	private static final String[] requiredUserDetails = new String[] { "mobile", "email", "user_name" };
		
	@Autowired
	private UserDetailsRepository userDetailsRepository;

	@Override
	public InitiatePaymentResponseDTO handleDepositRequest(DepositRequest d, Long userId) {
		UserDetails userDetails = userDetailsRepository.findUser(userId);
		
		if(userDetails == null) {
			throw new ValidationException(
					new ValidationResult<String>("Invalid User Id", ApiErrorCodes.INVALID_USERID));
		}
		
		if(userDetails.getEmail() == null) {
			userDetails.setEmail(paymentServiceConfiguration.getUserDefaultEmail());
		}
		
		return getInitiateResponse(d, userDetails);
	}

	@Override
	public String[] getRequiredUserDetails() {
		return requiredUserDetails;
	}

	@Override
	public GatewayIds getGatewayId() {
		return GatewayIds.CASHFREE;
	}

	private static final String HMAC_SHA256 = "HmacSHA256";

	private InitiatePaymentResponseDTO getInitiateResponse(DepositRequest paymentRequest, UserDetails userDetails) {
		InitiatePaymentResponseDTO response = new InitiatePaymentResponseDTO();
		response.setGatewayId(paymentRequest.getGatewayId());
		CashFreeDepositRequestParams cashFreeParams = new CashFreeDepositRequestParams();
		CashFreeGatewayConfig cashFreeGatewayConfig = paymentServiceConfiguration.getCashFreeGatewayConfig();
		cashFreeParams.setAppId(cashFreeGatewayConfig.getAppId());
		cashFreeParams.setOrderId(paymentRequest.getOrderId());
		cashFreeParams.setOrderAmount(paymentRequest.getAmount());
		cashFreeParams.setOrderCurrency(paymentRequest.getCurrency());
		cashFreeParams.setCustomerName(userDetails.getUserName());
		cashFreeParams.setCustomerEmail(userDetails.getEmail());
		cashFreeParams.setCustomerPhone(userDetails.getMobile());
		cashFreeParams.setNotifyURL(cashFreeGatewayConfig.getNotifyURL());
		cashFreeParams.setPaymentToken(generateSignature(paymentRequest, userDetails));
		response.setGatewayRequestParams(cashFreeParams);
		if (EnvironmentUtils.hasProdProfile(environment)) {
			response.setGatewayURL(cashFreeGatewayConfig.getProdURL());
		} else
			response.setGatewayURL(cashFreeGatewayConfig.getTestURL());
		return response;
	}
	
	private String generateSignature(DepositRequest paymentRequest, UserDetails userDetails) {
		Map<String, String> postData = new HashMap<String, String>();
		CashFreeGatewayConfig cashFreeGatewayConfig = paymentServiceConfiguration.getCashFreeGatewayConfig();
		postData.put("appId", cashFreeGatewayConfig.getAppId());
		postData.put("orderId", paymentRequest.getOrderId());
		postData.put("orderAmount", String.valueOf(paymentRequest.getAmount()));
		postData.put("orderCurrency", paymentRequest.getCurrency());
		postData.put("customerName", userDetails.getUserName());
		postData.put("customerEmail", userDetails.getEmail());
		postData.put("customerPhone", userDetails.getMobile());
		postData.put("notifyUrl", cashFreeGatewayConfig.getNotifyURL());
		String data = "";
		SortedSet<String> keys = new TreeSet<String>(postData.keySet());
		for (String key : keys) {
			data = data + key + postData.get(key);
		}
		Mac sha256_HMAC;
		try {
			sha256_HMAC = Mac.getInstance(HMAC_SHA256);
			SecretKeySpec secret_key_spec = new SecretKeySpec(cashFreeGatewayConfig.getSecretKey().getBytes(),
					HMAC_SHA256);
			sha256_HMAC.init(secret_key_spec);
			return Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(data.getBytes()));
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			throw new CashfreeSignaureGenerationException(e);
		}
	}
}