package com.rummyday.deposits.gateways.cashfree.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.rummyday.deposits.domain.models.UserDetails;

@Repository
public class UserDetailsRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate; 
    
    final String SELECT_BY_ID_QUERY = "SELECT user_name, email, mobile from users where id = ?";
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
        this.jdbcTemplate = jdbcTemplate;  
    }    

    public UserDetails findUser(Long userId) {
        return this.jdbcTemplate.queryForObject(SELECT_BY_ID_QUERY, new UserDetailMapper(), userId);
    }

    private static final class UserDetailMapper implements RowMapper<UserDetails> {
        public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        	UserDetails userDetails = new UserDetails();
            userDetails.setUserName(rs.getString("user_name"));
            userDetails.setEmail(rs.getString("email"));
            userDetails.setMobile(rs.getString("mobile"));
            return userDetails;
        }
    }    
}
