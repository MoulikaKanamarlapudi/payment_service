package com.rummyday.deposits.gateways.cashfree.exceptions;

import com.rummyday.deposits.dto.ApiErrorCodes;

public class CashfreeSignaureGenerationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CashfreeSignaureGenerationException(Throwable cause) {
		super(cause);
	}
	
	public String getErrorCode() {
		return ApiErrorCodes.CASHFREE_SIGNATURE_ERROR;
	}
}