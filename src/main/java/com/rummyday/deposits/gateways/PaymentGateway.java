package com.rummyday.deposits.gateways;

import com.rummyday.deposits.domain.models.DepositRequest;
import com.rummyday.deposits.domain.models.GatewayIds;
import com.rummyday.deposits.dto.InitiatePaymentResponseDTO;

public interface PaymentGateway {

	public InitiatePaymentResponseDTO handleDepositRequest(DepositRequest d, Long userId);
	
	public String[] getRequiredUserDetails();
	
	public GatewayIds getGatewayId();
	
}
