package com.rummyday.deposits.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rummyday.deposits.config.PaymentServiceConfiguration;
import com.rummyday.deposits.config.zookeeper.ZooKeeperConfig;
import com.rummyday.deposits.config.zookeeper.ZookeeperInit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ZookeeperService 
{
	@Autowired
	ZooKeeperConfig zooKeeperConfig;
	
	@Autowired
	ZookeeperInit config;
	
	@Autowired
	PaymentServiceConfiguration paymentServiceConfiguration; 
	
	public String reloadZK() throws Exception {
		log.info("Reloading Zookeeper Properties");
		config.reloadProperties();
		paymentServiceConfiguration.init();
		//TODO payment controller
		log.info("Reloaded Zookeeper Properties");
		return  "{\"status\":\"Reloaded Zookeeper Properties\"}";
		
	}
	

}
