package com.rummyday.deposits.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rummyday.deposits.config.PaymentServiceConfiguration;
import com.rummyday.deposits.domain.models.DepositRequest;
import com.rummyday.deposits.domain.models.GatewayIds;
import com.rummyday.deposits.dto.InitiatePaymentResponseDTO;
import com.rummyday.deposits.dto.InitiatePaymentsDTO;
import com.rummyday.deposits.dto.mappers.DTOMapper;
import com.rummyday.deposits.gateways.PaymentGateway;
import com.rummyday.deposits.gateways.PaymentGatewayRegistry;
import com.rummyday.deposits.repo.DepositRequestRepository;
import com.rummyday.payments.utils.OrderIdGenerator;

@Service
public class DepositService {
	

	@Autowired
	private DepositRequestRepository paymentRequestRepository;
	
	@Autowired
	private PaymentServiceConfiguration paymentServiceConfiguration;
	
	@Autowired
	private PaymentGatewayRegistry gatewayRegistry;
	
	@Autowired 
	private OrderIdGenerator orderIdGenerator;
	
	public InitiatePaymentResponseDTO initiatePayment(InitiatePaymentsDTO initiatePaymentsDTO) {

		DepositRequest p = DTOMapper.map(initiatePaymentsDTO);
		GatewayIds gateWayIds = chooseGateway(initiatePaymentsDTO, p);
		p.setOrderId(orderIdGenerator.generateNew());
		p.setCurrency(paymentServiceConfiguration.getOrderCurrency());
		PaymentGateway paymentGateway = gatewayRegistry.getPaymentGateway(gateWayIds);
		InitiatePaymentResponseDTO depositRequest = paymentGateway.handleDepositRequest(p, initiatePaymentsDTO.getUserId());
		DepositRequest savedRequest = paymentRequestRepository.save(p);
		depositRequest.setId(savedRequest.getId());
		return depositRequest;
	}
	
	private GatewayIds chooseGateway(InitiatePaymentsDTO dto, DepositRequest d) {
		GatewayIds gatewayId = GatewayIds.getFromPositon(dto.getGatewayId());
		if(gatewayId != null) {
			d.setGatewayId(gatewayId.getPosition());
		} else {
			//TODO gateway Selection based on gateway traffic and errors its suffering
			// select default gateway 
			gatewayId = paymentServiceConfiguration.getDefaultGateway();
			d.setGatewayId(gatewayId.getPosition());
		}			
		return gatewayId;
	}
	

	
	
}
