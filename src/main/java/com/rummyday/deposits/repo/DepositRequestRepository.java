package com.rummyday.deposits.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rummyday.deposits.domain.models.DepositRequest;

@Repository
public interface DepositRequestRepository extends JpaRepository<DepositRequest, Long>  {
	
	

}

