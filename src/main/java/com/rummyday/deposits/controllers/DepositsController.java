package com.rummyday.deposits.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rummyday.deposits.dto.InitiatePaymentResponseDTO;
import com.rummyday.deposits.dto.InitiatePaymentsDTO;
import com.rummyday.deposits.services.DepositService;
import com.rummyday.deposits.validators.ValidationException;

@RestController
@RequestMapping("payments/api")
public class DepositsController {
	@Autowired
	DepositService depositService;

	@RequestMapping(method = RequestMethod.POST, path = "/deposit")
	public ResponseEntity<InitiatePaymentResponseDTO> initiatePayment(
			@RequestBody InitiatePaymentsDTO initiatePaymentsDTO) throws ValidationException {
		InitiatePaymentResponseDTO initiatePaymentResponseDTO = depositService.initiatePayment(initiatePaymentsDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{orderId}")
				.buildAndExpand(initiatePaymentResponseDTO.getId()).toUri();
		return ResponseEntity.created(location).body(initiatePaymentResponseDTO);
	}

}