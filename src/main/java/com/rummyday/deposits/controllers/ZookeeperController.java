package com.rummyday.deposits.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rummyday.deposits.services.ZookeeperService;


@RestController
@RequestMapping(value = "/ls/api/zk")
public class ZookeeperController {
	@Autowired
	ZookeeperService zkService;

	@RequestMapping(value = "/zkReload", method = RequestMethod.GET, produces = "application/json")
	public String reloadZKProperties() throws Exception {
		return zkService.reloadZK();
	}

}
