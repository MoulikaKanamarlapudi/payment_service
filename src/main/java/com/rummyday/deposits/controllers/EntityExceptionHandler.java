package com.rummyday.deposits.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.rummyday.deposits.dto.ApiErrorCodes;
import com.rummyday.deposits.gateways.cashfree.exceptions.CashfreeSignaureGenerationException;
import com.rummyday.deposits.validators.ValidationException;

@ControllerAdvice
public class EntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({ ValidationException.class })
    public ResponseEntity<Object> handleBadRequest(final ValidationException ex, final WebRequest request) {
		ApiError error = new ApiError(ex.getErrorCode(), ex.getMessage());
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
	
	@ExceptionHandler({ CashfreeSignaureGenerationException.class })
    public ResponseEntity<Object> handleBadRequest(final CashfreeSignaureGenerationException ex, final WebRequest request) {
		ApiError error = new ApiError(ex.getErrorCode(), ex.getMessage());
        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	ApiError error = new ApiError(ApiErrorCodes.CANT_READ_INPUT_MESSAGE, ex.getMessage());
        return handleExceptionInternal(ex, error, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	ApiError error = new ApiError(ApiErrorCodes.INVALID_API_METHOD_ARGUMENTS, ex.getMessage());
        return handleExceptionInternal(ex, error, headers, HttpStatus.BAD_REQUEST, request);
    }	
}
