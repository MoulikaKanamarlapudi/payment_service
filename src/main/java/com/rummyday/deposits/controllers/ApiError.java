package com.rummyday.deposits.controllers;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiError {
	
	private final String errorCode;
	
	private final String erroMessage;
	
	private Map<String, Object> additionalData;
	
	public ApiError(String errorCode, String erroMessage) {
		this.errorCode = errorCode;
		this.erroMessage = erroMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErroMessage() {
		return erroMessage;
	}

	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}
	
	public void setAdditionalData(String key, Object value) {
		if(additionalData == null)
			additionalData = new HashMap<>();
		additionalData.put(key, value);
	}
}
