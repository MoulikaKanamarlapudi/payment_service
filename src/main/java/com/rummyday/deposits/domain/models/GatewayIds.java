package com.rummyday.deposits.domain.models;

public enum GatewayIds {
	
	CASHFREE(1);
	
	int position;

	GatewayIds(int position) {
		this.position = position;
	}
	
	public int getPosition() {
		return position;
	}
	
	public static GatewayIds getFromPositon(Integer position) {
		if(position == null) return null;
		for(GatewayIds gateWay: GatewayIds.values()) {
			if(gateWay.getPosition() == position)
				return gateWay;
		}
		return null;
	}

}
