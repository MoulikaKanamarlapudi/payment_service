package com.rummyday.deposits.domain.models;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rummyday.deposits.domain.converters.ZonedDateTimeAttributeConverter;

@Entity
@Table(name = "payment_init")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepositRequest implements Serializable{
	private static final long serialVersionUID = -4587868979015377582L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//	@RSQLAlias("id")
    private Long Id;
	
	@Column(name = "user_id")
	private long userId;
	
	@Column(name = "amount")
	private int amount;
	
	@Column(name = "order_id", unique = true)
	private String orderId;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "channel_id")
	private int channelId;
	
	@Column(name = "mode_option")
	private long modeOption;
	
	@Column(name = "gateway_id")
	private int gatewayId;
	

	@CreatedDate
	@Column(name = "record_date")
	@Convert(converter = ZonedDateTimeAttributeConverter.class)
	private ZonedDateTime createdDate = ZonedDateTime.now();
	
	@Column(name = "promo_code")
	private String promoCode;
	
	@Column(name = "promo_amount")
	private int promoAmount;
	
	public DepositRequest() {
		
	}
	
	public DepositRequest(Long Id) {
		this.Id = Id;
	}
	
	public Long getId() {
		return Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.Id = userId;
	}
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public Long getModeOption() {
		return modeOption;
	}

	public void setModeOption(Long modeOption) {
		this.modeOption = modeOption;
	}
	
	public int getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(int gatewayId) {
		this.gatewayId = gatewayId;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	public int getPromoAmount() {
		return promoAmount;
	}

	public void setPromoAmount(int promoAmount) {
		this.promoAmount = promoAmount;
	}
}
